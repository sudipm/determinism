#!/usr/bin/make

CC=gcc
CFLAGS=-Wall -O0

.PHONY: default
default: all

.PHONY: all
all: latency-measure

.PHONY: clean
clean:
	-rm -f latency-measure
	-rm -f results.dat
	-rm -f results.png

latency-measure: Makefile src/latency-measure.c
	${CC} ${CFLAGS} src/latency-measure.c -o $@
