# Project to investigate improving determinism for Linux Kernel

For some kinds of software workloads (including AI/ML workloads for autonomous
operation of robots and vehicles) it has been suggested that variability in OS
latency can affect overall performance, throughput and potentially quality.

This is an exploratory project to investigate the latency behaviour of Linux
kernels in a range of scenarios (multiple ISAs, stressed and unstressed, with
and without tweaks to improve latency and determinism for specific loads).

Our outputs are expected to include documentation, free software (ISC License) and results from various experiments which we aim to make reproducible.


## latency-measure
`latency-measure` will measure the latency of the system. It will record the time, do some work for a set time period, then record the time taken for the work to complete. The difference between the expected time and actual time is the latency.

### Compile
To compile, run

    make

### Run tests

To get usage information, run

    ./latency-measure --help    

### Results
`latency-measure` will output a file called `results.dat`, which contains the latency data. To produce a graph showing latency against time from these results, run

    ./src/latency_plot.sh

### Cycle Calibration

If you wish to run the same amount of time for each cycle in each workload, `test-mode` workload will help you to do it. When `./latency-measure -w test-mode` is executed the tool will perform operations to calculate the number of loops necessary to run 5ms cycles in each workload (except for usleep and nanosleep in which it can be manually defined). This will produce a file called by default `loop_values.dat` that will store the number of loops necessary. Finally, you only need to execute the desired workload and add the `-F` plus the loops file name.
For example: `./latency-measure -w register -d 5 -F loop_values.dat`


#### Run automated tests

To run automatically the system-mode-kill, system-mode-time, register-mode, memory-mode, usleep-mode and nanosleep-mode during a defined time use the run-tests script. The script will allow you to define the work mode, the duration of each test, the Linux scheduler, the task priority, to apply stress or not and to plot the results by providing to it the next arguments:
* -m  will define the work mode of the test: notaskset, taskset or test.
    - on notakset mode the run-tests script will execute each kind of test during the defined time.
    - on tasksetmode the script will run two different processes:
        - The first process will execute all the different tests, one by one sequentially on one CPU, when this finishes it will do the same with the next CPU and so on.
        - The second process will run the system-mode on CPU 1, the register-mode on CPU 2 and the memory-mode on CPU 3 in parallel.
    - on test mode the script will make use of `-w test-mode` workload of `latency-measure` tool to calculate the number of loops necessary to run each workload for 5ms in each CPU. This will create a file for each CPU, with the number of loops necessary for each CPU. DO NOT CHANGE THE NAME OF THE CREATED FILE, if the name of the file is changed the -F argument won't be able to work.
* -d will define how long is going to last each test.
* -s allows you select the Linux scheduler, the available schedulers are:"
 	 * o for SCHED_OTHER
 	 * f for SCHED_FIFO
 	 * r for SCHED_RR
 	 * b for SCHED_BATCH
 	 * i for SCHED_IDLE
 	 * d for SCHED_DEADLINE
* -p allows you select the task's priority
* -P allows you to automatically plot the results.
* -F load the files generated during test mode in order to run 5ms cycles in each workload
* -S Runs the test under stress using stress-ng

The test must be executed with superuser privileges

  sudo  ./run-tests -m taskset -d 10 -s f -p 20 -P        # run taskset mode, each executed test will last 10 seconds, the scheduler will be SCHED_FIFO, the priority will be 20 and results are going to be plotted
