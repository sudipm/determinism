#! /bin/bash

function plot (){
	echo -e "\nResults are going to get plotted\n"
	sh src/latency-plot.sh -i results-$2.dat -o results-$2.png &&
	sh src/latency-plot.sh -i results-$3.dat -o results-$3.png &&
	sh src/latency-plot.sh -i results-$4.dat -o results-$4.png &&
	if [ "$5" != "" ] ;
	then
	sh src/latency-plot.sh -i results-$5.dat -o results-$5.png &&
	mv results-$5.png $1/
	fi
	if [ "$6" != "" ] ;
	then
	sh src/latency-plot.sh -i results-$6.dat -o results-$6.png &&
	mv results-$6.png $1/
	fi
	if [ "$7" != "" ] ;
	then
	sh src/latency-plot.sh -i results-$7.dat -o results-$7.png &&
	mv results-$7.png $1/
	fi
	mv results-$2.png results-$3.png results-$4.png $1/
}

function bail()
{
	STATUS=0
	if [ "$1" ] ; then
		printf "$0: $*\n\n" 1>&2
		STATUS=1
	fi

	echo "Usage: $0 [OPTIONS]" 1>&2
	echo 'Available options:' 1>&2
	printf "\t-h              show this help\n" 1>&2
	printf "\t-m Mode         select working mode between taskset, notaskset, stress and test\n" 1>&2
	printf "\t-d duration     select the lenght of each test in seconds\n" 1>&2
	printf "\t-s scheduller   select the linux scheduller, SCHED_OTHER by default.\n" 1>&2
	printf "\t      the available schedullers are:\n"
	printf "\t	    o for SCHED_OTHER\n"
	printf "\t	    f for SCHED_FIFO\n"
	printf "\t	    r for SCHED_RR\n"
	printf "\t	    b for SCHED_BATCH\n"
	printf "\t	    i for SCHED_IDLE\n"
	printf "\t	    d for SCHED_DEADLINE\n"
	printf "\t-p priority     select the priority of the process, 0 by default\n" 1>&2
	printf "\t-P plot         select to plot the data by introducing "plot"\n" 1>&2
	printf "\t-F              load loops file\n"1>&2
	printf "\t-S Stress       Run tests under stress\n"1>&2
	exit ${STATUS}
}

function stress()
{
	if [ ${duration} -lt 120 ] ; then
		echo "In stress mode the minimun running time is for 120 seconds, the duration will be set to 120 seconds"
		duration=120
	fi 
	echo "Tests will run under stress"
	timeout=$(((${duration}/60)*6*3+7))
	if [ "${mode}" = "notaskset" ] ; then
		stress-ng --cpu 8 --io 4 --vm 2 --vm-bytes 128M --fork 4 --timeout ${timeout}m &
	elif [ "${mode}" = "taskset" ] ; then
		stress-ng --cpu 8 --io 4 --vm 2 --vm-bytes 128M --fork 4 --timeout ${timeout}m --taskset 1-3 &
	fi
}

duration="none"
mode="none"
f=" "
st_mode=""

while getopts ":hm:d:s:p:PFS" OPT ; do
	case "${OPT}" in
		\?)
			bail "Unsupported option ${OPTARG}"
		;;
		:)
			bail "Missing argument to option ${OPTARG}"
		;;
		m)
			mode=${OPTARG}
			if [ "${mode}" = "" ] ;	then
				echo "You have to select a working mode"
				exit
			fi
			if [ "${mode}" != "notaskset" -a "${mode}" != "taskset" -a "${mode}" != "stress" -a "${mode}" != "test" ] ; then
				bail "${mode} Unsupported"
			fi
		;;
		d)
			duration=${OPTARG}
			case "${duration}" in
				-*)
					echo "You have to select the duration of each test on seconds"
					bail
				;;
			
				*[^0-9]*)
					echo "You have to select the duration of each test on seconds"
					bail
				;;
			esac
			if [ ${duration} -le 0 ] ; then
				bail "Duration must be greater than 0"
			fi
		;;
		s)
			scheduler=${OPTARG}
			if [ "${scheduler}" != "o" -a "${scheduler}" != "f" -a "${scheduler}" != "r" -a "${scheduler}" != "b" -a \
				"${scheduler}" != "i" -a "${scheduler}" != "d" -a "${scheduler}" != "" ] ; then
				bail "${scheduler} Unsupported"
			fi
		;;
		p)
			priority=${OPTARG}
			if [  ${priority} -lt 0 ] ; then
				echo "Priority set to 0 by default"
				priority=0
			fi
		;;
		P)
			plot=y
		;;
		F)
			f="-F"
		;;
		S)
			st_mode="-stress"
		;;
		h)
			bail
		;;
		esac
done

if [ ${mode} == "none" ] ;
then
	echo "You have to select a working mode"
	bail
fi

if [ ${duration} == "none" -a ${mode} != "test" ] ;
then
	echo "You have to select the test running time"
	bail
fi


case "${scheduler}" in
	o)
		echo "The test will set the scheduling policy to SCHED_OTHER"
	;;
	f)
		echo "The test will set the scheduling policy to SCHED_FIFO"
	;;
	r)
		echo "The test will set the scheduling policy to SCHED_RR"
	;;
	b)
		echo "The test will set the scheduling policy to SCHED_BATCH"
	;;
	i)
		echo "The test will set the scheduling policy to SCHED_IDLE"
	;;
	d)
		echo "The test will set the scheduling policy to SCHED_DEADLINE"
	;;
esac

if [ "${scheduler}" = "" ] ; then
	echo "You haven't selected the scheduller. The tests will run using SCHED_OTHER, which is the default Linux policy."
	scheduler=o
fi

if [ "${priority}" = "" ] ; then
	priority=0
	echo "Priority set to 0 by default"
fi

if [ "${scheduler}" = "o" -o "${scheduler}" = "b" -o "${scheduler}" = "i" -o "${scheduler}" = "d" ] ; then
	if [ ${priority} != 0 ] ; then
		echo "The selected schedule can only run at priority 0."
		echo "The Scheduler's permited priority values are:"
		echo "SCHED_OTHER min/max priority	: 0/0"
		echo "SCHED_FIFO min/max priority	: 1/99"
		echo "SCHED_RR min/max priority	: 1/99"
		echo "SCHED_BATCH min/max priority	: 0/0"
		echo "SCHED_IDLE min/max priority	: 0/0"
		echo "SCHED_DEADLINE min/max priority	: 0/0"
		echo "Priority will be set to 0."
		priority=0
	fi
fi

if [ ${priority} -gt 99 ]; then
	echo "The scheduler priority can't be greater than 99"
	echo "The priority will be set to 99"
	priority=99
fi



case ${mode} in
	"notaskset")
	 	echo -e "\n\nnotaskset mode tests will be executed\n\n"
		mkdir -p notaskset-tests$st_mode
		if [ "$st_mode" = "-stress" ] ; then
			stress
		fi
		if [ "${f}" = "-F" ] ; then
			file="values-all.dat"
		fi
		chrt -$scheduler $priority ./latency-measure -w usleep -d $duration -f results-usleep.dat $f $file &&
		date
		chrt -$scheduler $priority ./latency-measure -w register -d $duration -f results-register.dat $f $file &&
		date
		chrt -$scheduler $priority  ./latency-measure -w memory -d $duration -f results-memory.dat $f $file &&
		date
		chrt -$scheduler $priority  ./latency-measure -w system-kill -d $duration -f results-system-kill.dat $f $file &&
		date
		chrt -$scheduler $priority  ./latency-measure -w nanosleep -d $duration -f results-nanosleep.dat $f $file &&
		date
		chrt -$scheduler $priority  ./latency-measure -w system-time -d $duration -f results-system-time.dat $f $file &&
		date
		if [ "${plot}" = "y" ] ; then
			plot notaskset-tests usleep register memory system-kill system-time nanosleep
		fi
		mv results-system-time.dat results-system-kill.dat results-usleep.dat results-register.dat results-memory.dat results-nanosleep.dat notaskset-tests$st_mode/
		exit
	;;

	"taskset")
		echo -e "\n\ntaskset mode tests will be executed\n\n"
		mkdir -p taskset-tests$st_mode
		if [ "$st_mode" = "-stress" ] ; then
			stress
		fi
		for ((i=1;i<4;i++));
			do
				if [ "${f}" = "-F" ] ; then
					file="values-c$i.dat"
				fi
				echo -e "\nRunning tests on CPU $i\n"
				mkdir $i-core
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w usleep -d $duration -f results-usleep.dat $f $file &&
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w register -d $duration -f results-register.dat $f $file &&
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w memory -d $duration -f results-memory.dat $f $file &&
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w system-kill -d $duration -f results-system-kill.dat $f $file &&
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w system-time -d $duration -f results-system-time.dat $f $file &&
				date
				chrt -$scheduler $priority  taskset -c $i	./latency-measure -w nanosleep -d $duration -f results-nanosleep.dat $f $file &&
				date
				if [ "${plot}" = "y" ] ; then
					plot $i-core usleep register memory system-kill system-time nanosleep
				fi
				mv results-system-time.dat results-system-kill.dat results-usleep.dat results-register.dat results-memory.dat results-nanosleep.dat $i-core/
		done

		mkdir -p 1-3-core
		echo -e "\nRunning system-mode on CPU 1, register-mode on CPU2 and memory-mode on CPU3\n"
		date
		chrt -$scheduler $priority  taskset -c 1 ./latency-measure -w system-kill -d $duration -f results-system-kill.dat $f $file &
		chrt -$scheduler $priority  taskset -c 2 ./latency-measure -w register -d $duration -f results-register.dat  $f $file &
		chrt -$scheduler $priority  taskset -c 3 ./latency-measure -w memory -d $duration -f results-memory.dat $f $file &
		wait
		date
		if [ "${plot}" = "y" ] ; then
			plot 1-3-core register memory system-kill
		fi
		mv results-system-kill.dat results-register.dat results-memory.dat  1-3-core/
		mv 1-core/ 2-core/ 3-core/ 1-3-core/ taskset-tests$st_mode
		exit
	;;

	"test")
		echo -e "\n\ntest mode will be executed\n\n"
		chrt -$scheduler $priority ./latency-measure -w test-mode -f values-all.dat
		for ((i=1;i<4;i++));
			do
				echo -e "\nRunning tests on CPU $i\n"
				date
				chrt -$scheduler $priority  taskset -c $i ./latency-measure -w test-mode -f values-c$i.dat
				date
		done
		exit
	;;

esac
