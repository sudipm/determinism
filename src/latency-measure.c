/* latency-measure.c - Programme to measure latency
*
* Copyright 2018 Codethink Ltd
*
* For licence terms, see the LICENSE file which accompanies this software.
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <unistd.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>
#include <ctype.h>

#define DEFAULT_FILENAME "results.dat"
#define DEFAULT_PERIOD 1000 /* microseconds */
#define NSEC_PER_SEC 1000000000L

#define DEFAULT_LOOPS 100000 //DEFINES THE NUMBER OF TIMES THAT OPERATIONS WILL BE EXECUTED PER ITERATION IN SYSTEM_MODE AND REGISTER_MODE
#define DEFAULT_M_LOOPS 10000 ////DEFINES THE NUMBER OF TIMES THAT OPERATIONS WILL BE EXECUTED PER ITERATION IN MEMORY_MODE
#define WORKSPACE_SIZE 128 //DEFINES THE SIZE OF WORKSPACE[]

#define USLEEP_MODE         0
#define MEMORY_MODE         1
#define REGISTER_MODE       2
#define SYSTEM_MODE_KILL    3
#define NANOSLEEP_MODE      4
#define SYSTEM_MODE_TIME    5
#define TEST_MODE           6

#define CPUFREQ_FMT_STR "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_cur_freq"

const char *workModeStr[7] = {"usleep", "memory", "register", "system-kill", "nanosleep", "system-time", "test-mode"};

static void printHelp(char name[]) {
  printf("Usage: %s [OPTIONS]\n", name);
  printf(" -w, --work-mode     Type of work to be performed. Options:\n");
  printf("                         %s : put the system to sleep with usleep()\n", workModeStr[USLEEP_MODE]);
  printf("                         %s : put the system to sleep with clock_nanosleep()\n", workModeStr[NANOSLEEP_MODE]);
  printf("                         %s : do operations accessing memory\n", workModeStr[MEMORY_MODE]);
  printf("                         %s : do operations in registers\n", workModeStr[REGISTER_MODE]);
  printf("                         %s : do operations (kill()) accessing kernel\n", workModeStr[SYSTEM_MODE_KILL]);
  printf("                         %s : do operations (clock_gettime) accessing kernel\n", workModeStr[SYSTEM_MODE_TIME]);
  printf("                         %s : execute a Test to calibrate the tests cycle time to 5ms\n", workModeStr[TEST_MODE]);
  printf(" -d, --duration T    Run the program for T seconds\n");
  printf(" -f, --filename F    Use alternate filename (default %s)\n", DEFAULT_FILENAME);
  printf(" -m, --clock-monotonic  Use monotonic clock as time source (default)\n");
  printf(" -r, --clock-realtime   Use realtime clock as time source\n");
  printf(" -p, --period N      Take a sample every N microseconds (default is %d)\n", DEFAULT_PERIOD);
  printf(" -l, --loops L      Number of loops of the workload (default is %d for Memory-mode and %d for system-mode and register-mode)\n", DEFAULT_M_LOOPS, DEFAULT_LOOPS);
  printf(" -F  --FILE         Read from file the number of loops to be performed to achieve 5ms cycle time tests");
  printf(" -h, --help          Print this help and exit\n");
  printf("\n");
  printf("Example: %s --work-mode sleep --duration 5 --period 2000\n", name);
  printf("\n");
}


int register_bound(int count) {
  int arg = count;
  int i = 0;
  for (i = 0; i < count; i++) {
    arg *= i;
    arg %= 0xCAFEBABE;
  }
  return arg;
}

int memory_bound(int count) {
  int workspace [WORKSPACE_SIZE];
  int i = 0, j = 0;
  for (i = 0; i < WORKSPACE_SIZE; i++) {
    workspace[i] = count + 1;
  }
  for (i = 0; i < count; i++) {
    for (j = 0; j < WORKSPACE_SIZE; j++) {
      workspace[j] = workspace[(i + 1) % WORKSPACE_SIZE] * workspace[(i + 3) % WORKSPACE_SIZE];
    }
  }
  return workspace[count % WORKSPACE_SIZE];
}

void sys_call_kill (int count) {
  /*Make a system call with kill. The kill(pid, sig) system call can be used to send any signal to any process group or process.
  If pid equals 0, then sig is sent to every process in the process group of the calling process.
  If sig is 0, then no signal is sent, but existence and permission checks are still performed; this can be used to check for the existence of a process ID or process group ID that the caller is
  permitted to signal.*/
  int i = 0;
  for (i = 0; i < count; i++) {
    kill(0, 0);
  }
}

void sys_call_time (int count, clockid_t clocksource, struct timespec t_current) {
  int i = 0;
  for (i = 0; i < count; i++) {
    clock_gettime(clocksource, &t_current);
  }
}

struct cpu_freq_ctx {
  struct {
    char *path;
    FILE *file;
    unsigned int read_count;
    unsigned int measured;
    unsigned int max;
    unsigned int min;
    uint64_t total;
  } *cpu;
  unsigned cpu_count;
};

static void cpu_freq_init(struct cpu_freq_ctx *ctx)
{
  unsigned int i;
  FILE *cpus_present;
  unsigned min_cpu = 0;
  unsigned max_cpu = 3;

  assert(ctx != NULL);

  memset(ctx, 0, sizeof(*ctx));

  cpus_present = fopen("/sys/devices/system/cpu/possible", "r");
  if (cpus_present != NULL) {
    unsigned temp_min_cpu;
    unsigned temp_max_cpu;
    if (fscanf(cpus_present, "%u-%u", &temp_min_cpu, &temp_max_cpu) == 2) {
      if (max_cpu >= min_cpu) {
        min_cpu = temp_min_cpu;
        max_cpu = temp_max_cpu;
      }
    }
    fclose(cpus_present);
  }

  ctx->cpu_count = max_cpu - min_cpu + 1;
  ctx->cpu = calloc(ctx->cpu_count, sizeof(*ctx->cpu));
  if (ctx->cpu == NULL) {
    ctx->cpu_count = 0;
  }

  for (i = 0; i < ctx->cpu_count; i++) {
    char *path;
    ssize_t len;
    ctx->cpu[i].min = UINT_MAX;
    len = snprintf(NULL, 0, CPUFREQ_FMT_STR, min_cpu + i);
    if (len < 0) {
      continue;
    }
    path = malloc(len + 1);
    if (path == NULL) {
      continue;
    }
    if (snprintf(path, len + 1, CPUFREQ_FMT_STR, min_cpu + i) != len) {
      free(path);
      continue;
    }

    ctx->cpu[i].path = path;
  }
}

static void cpu_freq_open(struct cpu_freq_ctx *ctx)
{
  unsigned int i;

  assert(ctx != NULL);

  for (i = 0; i < ctx->cpu_count; i++) {
    if (ctx->cpu[i].path == NULL) {
      continue;
    }
    ctx->cpu[i].file = fopen(ctx->cpu[i].path, "r");
  }
}

static void cpu_freq_read(struct cpu_freq_ctx *ctx)
{
  unsigned int i;

  assert(ctx != NULL);

  for (i = 0; i < ctx->cpu_count; i++) {
    unsigned int measured;

    if ((ctx->cpu[i].file == NULL) ||
        (fscanf(ctx->cpu[i].file, "%u", &measured) != 1)) {
      ctx->cpu[i].measured = 0;
      continue;
    }

    /* Update ctx with new measurement. */
    ctx->cpu[i].measured = measured;
    ctx->cpu[i].total += measured;
    if (measured > ctx->cpu[i].max) {
      ctx->cpu[i].max = measured;
    }
    if (measured < ctx->cpu[i].min) {
      ctx->cpu[i].min = measured;
    }

    ctx->cpu[i].read_count++;
  }
}

static void cpu_freq_print(const struct cpu_freq_ctx *ctx)
{
  unsigned int i;

  assert(ctx != NULL);

  printf("CPU frequency values (KHz) are:\n");
  printf("\t   Minimum         Maximum          Mean \n");

  for (i = 0; i < ctx->cpu_count; i++) {
    if (ctx->cpu[i].file == NULL) {
      continue;
    }

    printf("CPU%d\t %9u       %9u      %9" PRIu64 "\n", i,
           ctx->cpu[i].min, ctx->cpu[i].max,
           ctx->cpu[i].total / ctx->cpu[i].read_count);
  }
}

static void cpu_freq_dump(const struct cpu_freq_ctx *ctx, FILE *output)
{
  unsigned int i;

  assert(ctx != NULL);

  for (i = 0; i < ctx->cpu_count; i++) {
    fprintf(output, " %u", ctx->cpu[i].measured);
  }
}

static void cpu_freq_close(struct cpu_freq_ctx *ctx)
{
  unsigned int i;

  assert(ctx != NULL);

  for (i = 0; i < ctx->cpu_count; i++) {
    if (ctx->cpu[i].file != NULL) {
      fclose(ctx->cpu[i].file);
    }
  }
}

static void cpu_freq_fini(struct cpu_freq_ctx *ctx)
{
  unsigned int i;

  assert(ctx != NULL);

  for (i = 0; i < ctx->cpu_count; i++) {
    free(ctx->cpu[i].path);
  }
  free(ctx->cpu);
}

static inline void AddMicroSeconds(struct timespec* startTime, uint64_t microseconds) {
  uint64_t nanoseconds = microseconds * 1000;
  nanoseconds += startTime->tv_nsec;
  startTime->tv_sec += (nanoseconds / NSEC_PER_SEC);
  startTime->tv_nsec = nanoseconds % NSEC_PER_SEC;
}

int do_work(char workMode, int duration, int period, clockid_t clocksource, unsigned int loops [4], FILE *f, int check){

  int ret = 1;
  int  iterations = 0;
  uint64_t totalLatency = 0;
  uint64_t minLatency = UINT_MAX;
  uint64_t maxLatency = 0;
  uint64_t meanLatency = 0;
  struct cpu_freq_ctx ctx;

  cpu_freq_init(&ctx);

  // Get the start time
  struct timespec t_start, ticker;
  struct timespec t_previous, t_current;
  clock_gettime(clocksource, &t_start);
  t_current = t_start;

  while (t_current.tv_sec < (t_start.tv_sec + duration)) {
    int64_t microsec, nsecs_previous, nsecs_current;
    uint64_t latency;
    cpu_freq_open(&ctx);

    // Read clock -> do work -> read clock
    clock_gettime(clocksource, &t_previous);
    switch (workMode){

      case NANOSLEEP_MODE:
        ticker = t_previous;
        AddMicroSeconds(&ticker, period);
        clock_nanosleep(clocksource, TIMER_ABSTIME, &ticker, NULL);
      break;

      case USLEEP_MODE:
        usleep(period);
      break;

      case REGISTER_MODE:
        register_bound(loops[0]);
      break;

      case MEMORY_MODE:
        memory_bound(loops[1]);
      break;

      case SYSTEM_MODE_KILL:
        sys_call_kill (loops[2]);
      break;

      case SYSTEM_MODE_TIME:
        sys_call_time (loops[3], clocksource, t_current);
      break;

    }
    clock_gettime(clocksource, &t_current);

    nsecs_previous = (int64_t)t_previous.tv_sec * NSEC_PER_SEC
                    + t_previous.tv_nsec;
    nsecs_current = (int64_t)t_current.tv_sec * NSEC_PER_SEC
                    + t_current.tv_nsec;

    // Calculate latency (in microseconds) and related statistics
    latency = (nsecs_current - nsecs_previous)/1000;
    if (latency > maxLatency)
      maxLatency = latency;
    if (latency < minLatency)
      minLatency = latency;

    totalLatency += latency;
    iterations++;

    // Read processor frequency and calculate max, min and mean values
    cpu_freq_read(&ctx);
    cpu_freq_close(&ctx);

    microsec = nsecs_previous/1000;

    if (check != 2){
      fprintf(f, "%" PRId64 " %" PRIu64 " ", microsec, latency);
      cpu_freq_dump(&ctx, f);
      fprintf(f, "\n");
    }
  }

  meanLatency = totalLatency / iterations;


   //check == 2 makes sure that it's only entered into the loop when the program is only running int test_mode
   if (check == 2){
     unsigned int loops_values;

     loops_values = (5000 * DEFAULT_LOOPS) /  meanLatency;
     fprintf(f, "%u ", loops_values);
     ret = loops_values;
   }
   else {
     printf("\nRecording complete.");
     printf("\n");
     printf("Latency values are:\n");
     printf("\tMinimum: %" PRIu64 "us\n", minLatency);
     printf("\tMaximum: %" PRIu64 "us\n", maxLatency);
     printf("\tMean   : %" PRIu64 "us\n", meanLatency);

     printf("\n");
     cpu_freq_print(&ctx);
  }

  cpu_freq_fini(&ctx);
  return ret;
}


int main(int argc, char *argv[] ) {
  char *filename = DEFAULT_FILENAME;
  clockid_t clocksource= CLOCK_MONOTONIC;
  int retcode = EXIT_SUCCESS;
  int c;
  int duration = -1, period = -1;
  int workMode = -1;
  unsigned int loops [4] = {0, 0, 0, 0};
  int i, check = 0, f_check=0;
  FILE * f; //Create file
  FILE *values;

  // Parse cmdline arguments
  const char    * short_opt = "hd:f:mp:rw:l:F:";
  struct option   long_opt[] =
  {
     {"help",          no_argument,       NULL, 'h'},
     {"duration",      required_argument, NULL, 'd'},
     {"filename",      required_argument, NULL, 'f'},
     {"clock-monotonic",  no_argument,    NULL, 'm'},
     {"clock-realtime",   no_argument,    NULL, 'r'},
     {"period",        required_argument, NULL, 'p'},
     {"work-mode",       required_argument, NULL, 'w'},
     {"loops",       required_argument, NULL, 'l'},
     {"FILE",       required_argument, NULL, 'F'},
     {NULL,            0,                 NULL, 0  }
  };

  while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
    switch (c) {
      case -1:       /* no more arguments */
      case 0:        /* long options toggles */
      break;

      case 'd':
        duration = atoi(optarg);
      break;

      case 'f':
        filename = optarg;
        f_check = 1;
        break;

      case 'm':
        clocksource= CLOCK_MONOTONIC;
        break;

      case 'p':
        period = atoi(optarg);
      break;

      case 'r':
        clocksource= CLOCK_REALTIME;
        break;

      case 'h':
        printHelp(argv[0]);
        return 0;
      break;

      case 'w':
        if (!strcmp(optarg, workModeStr[USLEEP_MODE]))
          workMode = USLEEP_MODE;
          else if (!strcmp(optarg, workModeStr[NANOSLEEP_MODE]))
            workMode = NANOSLEEP_MODE;
        else if (!strcmp(optarg, workModeStr[MEMORY_MODE]))
          workMode = MEMORY_MODE;
        else if (!strcmp(optarg, workModeStr[REGISTER_MODE]))
          workMode = REGISTER_MODE;
        else if (!strcmp(optarg, workModeStr[SYSTEM_MODE_KILL]))
          workMode = SYSTEM_MODE_KILL;
        else if (!strcmp(optarg, workModeStr[SYSTEM_MODE_TIME]))
          workMode = SYSTEM_MODE_TIME;
        else if (!strcmp(optarg, workModeStr[TEST_MODE])){
          workMode = TEST_MODE;
          }
        else {
          fprintf(stderr, "%s: invalid option -w %s\n", argv[0], optarg);
          fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
          return(-2);
        }
      break;

      case 'l':
        for (i=0; i < 4; i ++){
          loops [i] = atoi(optarg);
        }
      break;

      case 'F':
        values = fopen(optarg, "r");
        if (values == NULL) {
          printf("Error opening file %s\n", optarg);
          return -1;
        }
        else {
          check = 1;
        }
      break;

      default:
        fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
        fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
        return(-2);
    };
  };

  // Check the duration argument
  if (duration <= 0 && workMode != TEST_MODE) {
    fprintf(stderr, "Needed : duration in seconds\n\n");
    printHelp(argv[0]);
    return -2;
  }

  // Check the period argument
  if (period <= 0) {
    period = DEFAULT_PERIOD;
  }

  // Check the work mode argument
  if (workMode < 0) {
    fprintf(stderr, "Needed : work mode\n");
    printHelp(argv[0]);
    return -2;
  }

  //Open file
  if((workMode != TEST_MODE) || ((workMode == TEST_MODE) && (f_check == 1))){
    f = fopen(filename, "w+");
    if (f == NULL) {
      printf("Error opening file %s\n", filename);
      return -1;
    }
  }else{
    filename = "loop_values.dat";
    f = fopen(filename, "w+");
    if (f == NULL) {
      printf("Error opening file %s\n", filename);
      return -1;
    }
  }


  // check the number of loops
    if(check == 1 && workMode != TEST_MODE){
      if(fscanf(values, "%u %u %u %u", &loops[0], &loops[1], &loops[2], &loops[3]) != 4){
        printf("Unexpected end of file\n");
        return -1;
      }
      else if ((loops[0] < 1) || (loops [0] > 2000000) || (loops[1] < 1) || (loops [1] > 2000000) || (loops[2] < 1) ||
      (loops [2] > 2000000) || (loops[3] < 1) || (loops [3] > 2000000)){
        printf("Value out of range\n");
        return -1;
      }
      else if (getc(values) != EOF){
        while(isspace(getc(values)));
        if (getc(values) != EOF){
          printf("EOF not reached, corrupted data\n");
          return -1;
        }
      }
      fclose(values);
  }
  else if (check == 1 && workMode == TEST_MODE){
    printf("-F is not compatible with test-mode\n");
    return -1;
  }
  else if (workMode == TEST_MODE) {
    loops [0] = DEFAULT_LOOPS;
    loops [1] = DEFAULT_LOOPS;
    loops [2] = DEFAULT_LOOPS;
    loops [3] = DEFAULT_LOOPS;
  }
  else{
    if (loops [0] <= 0) {
      loops [0] = DEFAULT_LOOPS;
      loops [1] = DEFAULT_M_LOOPS;
      loops [2] = DEFAULT_LOOPS;
      loops [3] = DEFAULT_LOOPS;
      }
  }

  // Print useful infor on how the programme is running
  printf("Recording latency measurements into %s. ", filename);
  if (workMode == USLEEP_MODE){
    printf("The system will be put to sleep using usleep() for %dus periods to measure the latency.", period);
  }
  if (workMode == NANOSLEEP_MODE){
    printf("The system will be put to sleep using clock_nanosleep() for %dus periods to measure the latency.", period);
  }
  if (workMode == MEMORY_MODE){
    printf("The programme will perform memory access operations to measure the latency.\n");
    printf("The workload will perform %u loops per iteration", loops[1]);
  }
  if (workMode == REGISTER_MODE){
    printf("The programme will perform register operations to measure the latency.");
    printf("The workload will perform %u loops per iteration", loops[0]);
  }
  if (workMode == SYSTEM_MODE_KILL){
    printf("The programme will perform kernel operations (kill()) to measure the latency.");
    printf("The workload will perform %u loops per iteration", loops[2]);
  }
  if (workMode == SYSTEM_MODE_TIME){
    printf("The programme will perform kernel operations (clock_gettime()) to measure the latency.\n");
    printf("The workload will perform %u loops per iteration", loops[3]);
  }
  if (workMode == TEST_MODE){
    printf("The programme will perform operations to calibrate the cycle time to 5ms.\n");
    printf("The workload will perform %u loops for all workloads per iteration.", loops[0]);
    duration = 60;
  }

  if (workMode != TEST_MODE)
    printf(" %ds duration, ", duration);
  else {
      printf(" Each workload will run for %ds, ", duration);
    }
  if (clocksource == CLOCK_REALTIME)
    printf("using realtime clock source.\n\n");
  else if (clocksource == CLOCK_MONOTONIC)
    printf("using monotonic clock source.\n\n");

  if (workMode != TEST_MODE){
    do_work(workMode, duration, period, clocksource, loops, f, check);
  }
  //check != 1 makes sure that it's only entered into the "else if" if the file with the loops values has not been opened
  else if (check == 0 && workMode == TEST_MODE){
    char workMode_Test[4] = {REGISTER_MODE, MEMORY_MODE, SYSTEM_MODE_KILL, SYSTEM_MODE_TIME};
    unsigned int wloop = 0;
    check = 2;
    for(i = 0; i < 4; i++){
      wloop = do_work(workMode_Test[i], duration, period, clocksource, loops, f, check);
      switch (workMode_Test[i]){
        case REGISTER_MODE:
              printf("%u\t loops must be executed to achieve 5ms cycles for register-bound\n", wloop);
              break;
        case MEMORY_MODE:
              printf("%u\t loops must be executed to achieve 5ms cycles for memory-bound\n", wloop);
              break;
        case SYSTEM_MODE_KILL:
              printf("%u\t loops must be executed to achieve 5ms cycles for system-kill\n", wloop);
              break;
        case SYSTEM_MODE_TIME:
              printf("%u\t loops must be executed to achieve 5ms cycles for system-time\n", wloop);
              break;
      }
    }
    printf("\nRecording complete.\n");
  }
  fclose(f);
  return retcode;
}
